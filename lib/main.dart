import 'package:flutter/material.dart';
import 'package:calculator_app/HomePage.dart';

void main(){

  runApp(new MyApp());

}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(

      theme: new ThemeData(

        accentColor: Colors.red,
        primarySwatch: Colors.red

      ),
      title: "Calculator",
      home: new HomePage(),

    );
  }
}
