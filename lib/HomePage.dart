import 'package:flutter/material.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {

  var num1 , num2  , sum = 0;


  final TextEditingController t1 = new TextEditingController(text: "0");
  final TextEditingController t2 = new TextEditingController(text: "0");



  void doAddition(){

    setState(() {

      num1 = int.parse(t1.text);
      num2 = int.parse(t2.text);

      sum = num1 + num2;

    });
  }

  void doSubtraction(){

    setState(() {

      num1 = int.parse(t1.text);
      num2 = int.parse(t2.text);

      sum = num1 - num2;

    });
  }

  void doMultiply(){

    setState(() {

      num1 = int.parse(t1.text);
      num2 = int.parse(t2.text);

      sum = num1 * num2;

    });
  }

  void doDivide(){

    setState(() {


      num1 = int.parse(t1.text);
      num2 = int.parse(t2.text);

      try{
        sum = num1 ~/ num2;
      }on IntegerDivisionByZeroException{


      }


    });
  }

  void clear(){

    setState(() {


     t1.text = "0";
     t2.text = "0";

    });
  }





  @override
  Widget build(BuildContext context) {
    return new Scaffold(

      appBar: new AppBar(

        title: new Text("Calculator"),
        backgroundColor: Colors.red,
      ),

      body: new Container(
        
        padding: const EdgeInsets.all(20.0),


        
        child: new Column(
          
          mainAxisAlignment: MainAxisAlignment.center,

          children: <Widget>[

            new Text("Output : $sum" ,

            style: new TextStyle(
              fontSize: 20.0,
              color: Colors.black,
              fontWeight: FontWeight.bold

            ),
            ),




            new TextField(

              keyboardType: TextInputType.number,
              controller: t1,
              decoration: new InputDecoration(
                hintText: "Enter number 1 ",


              ),
            ),

            new TextField(

              controller: t2,
              keyboardType: TextInputType.number,
              decoration: new InputDecoration(

                 

                  hintText: "Enter number 2 "

              ),
            ),
            
            new Padding(padding: const EdgeInsets.only(top: 20.0)),

            new Row(

              mainAxisAlignment: MainAxisAlignment.center,

              children: <Widget>[

                new MaterialButton(


                  child: new Text("+"),
                  textColor: Colors.white,
                  color: Colors.red,
                  onPressed: doAddition,
                ),

                new Padding(padding: const EdgeInsets.only(right: 20.0)),

                new MaterialButton(


                  child: new Text("X"),
                  textColor: Colors.white,
                  color: Colors.red,
                  onPressed:doMultiply,
                ),
              ],
            ),

                new Padding(padding: const EdgeInsets.only(top: 20.0),),

                new Row(

                  mainAxisAlignment: MainAxisAlignment.center,

                  children: <Widget>[

                    new MaterialButton(




                      child: new Text("-"),
                      textColor: Colors.white,

                      color: Colors.red,
                      onPressed: doSubtraction,
                    ),

                    new Padding(padding: const EdgeInsets.only(right: 20.0)),

                    new MaterialButton(


                      child: new Text("%"),
                      textColor: Colors.white,
                      color: Colors.red,
                      onPressed: doDivide,
                    ),

                  ],
                ),

            new Padding(padding: const EdgeInsets.only(top: 40.0)),

            new Row(

              mainAxisAlignment: MainAxisAlignment.center,

              children: <Widget>[

                new MaterialButton(


                  child: new Text("Clear"),
                  textColor: Colors.white,
                  color: Colors.red,
                  onPressed: clear,
                ),

              ],
            )







          ],

        ),
      ),


    );
  }
}
